const express = require("express");
const data = require("./data/data.json");
const app = express();

app.use(function (_req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/data", function (_req, res, _next) {
  res.send(data);
});

app.listen(5000, () => console.warn("Server is running on port 5000"));
