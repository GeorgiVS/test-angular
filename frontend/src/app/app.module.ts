import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TableComponent } from './table/components/table.component';
import { SearchComponent } from './ui/search/components/search.component';
import { PaginationComponent } from './ui/pagination/components/pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    SearchComponent,
    PaginationComponent,
  ],
  imports: [BrowserModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
