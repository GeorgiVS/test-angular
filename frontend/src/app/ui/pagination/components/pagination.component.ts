import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IPagination } from './../data/interfaces';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./styles/pagination.component.scss'],
})
export class PaginationComponent {
  public pagesArray: Array<number> = [];
  public currentPage: number = 1;

  @Input() set setPagination(pagination: IPagination) {
    if (pagination) {
      const pagesAmount = Math.ceil(
        pagination.itemsCount / pagination.pageSize
      );
      this.pagesArray = new Array(pagesAmount).fill(1);
    }
  }

  @Output() goToPage = new EventEmitter();

  public setPage(pageNumber: number): void {
    if (pageNumber === this.currentPage) return;
    this.currentPage = pageNumber;
    this.goToPage.emit(pageNumber);
  }
}
