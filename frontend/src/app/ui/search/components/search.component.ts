import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { debounceTime, Subject } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnDestroy {
  @Output() searchValue: EventEmitter<string> = new EventEmitter();

  private _searchSubject: Subject<string> = new Subject();

  constructor() {
    this._setSearchSubscription();
  }

  public updateSearch(event: Event) {
    this._searchSubject.next((event.target as HTMLInputElement).value);
  }

  private _setSearchSubscription() {
    this._searchSubject
      .pipe(debounceTime(500))
      .subscribe((searchValue: string) => {
        this.searchValue.emit(searchValue);
      });
  }

  ngOnDestroy() {
    this._searchSubject.unsubscribe();
  }
}
