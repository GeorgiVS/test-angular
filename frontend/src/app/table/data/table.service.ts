import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TableService {
  protected API_URL = 'http://localhost:5000'; //В .env по хорошему перенести

  constructor(private _http: HttpClient) {}

  public getTable(): Observable<any> {
    return this._http.get(`${this.API_URL}/data`).pipe(
      catchError((error: Error) => {
        return throwError(error);
      })
    );
  }
}
