import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ITable } from '../data/interfaces';
import { TableService } from './../data/table.service';
import { IPagination } from './../../ui/pagination/data/interfaces';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./styles/table.component.scss'],
})
export class TableComponent implements OnInit, OnDestroy {
  public tableTitles = [
    'availability',
    'brandId',
    'brandName',
    'id',
    'image',
    'name',
    'nomenclature',
    'ordered',
    'orderedAmount',
    'preview',
    'reviewsCount',
    'sku',
    'soldAmount',
    'soldQuantity',
    'wbRating',
  ];

  public currentTable: ITable[] = [];
  public pagination: IPagination = { itemsCount: 0, pageSize: 5 };
  public pageRange = { min: 0, max: this.pagination.pageSize };
  protected unsubscriber: Subject<void> = new Subject<void>();
  private _lastSort = { key: '', ascending: false };
  private _initialTable: ITable[] = [];

  constructor(public tableService: TableService) {}

  public ngOnInit(): void {
    this.tableService
      .getTable()
      .pipe(takeUntil(this.unsubscriber))
      .subscribe((table: ITable[]) => {
        this._initialTable = table;
        this.currentTable = table;
        this.pagination.itemsCount = table.length;
      });
  }

  public ngOnDestroy(): void {
    this.unsubscriber.next();
  }

  public searchValue(value: string) {
    this.currentTable = this._initialTable.filter(
      (item) =>
        item.brandName.toUpperCase().includes(value.toUpperCase()) ||
        item.name.toUpperCase().includes(value.toUpperCase())
    );

    this.pagination = {
      itemsCount: this.currentTable.length,
      pageSize: 5,
    };

    this.sort(this._lastSort.key, this._lastSort.ascending);
  }

  public visibleSort(key: string): boolean {
    return !![
      'availability',
      'nomenclature',
      'ordered',
      'ordered',
      'orderedAmount',
      'reviewsCount',
      'soldAmount',
      'soldQuantity',
      'wbRating',
    ].find((item: string) => item === key);
  }

  public sort(key: string, ascending: boolean = false): void {
    this.currentTable = this.currentTable.sort((a: any, b: any) =>
      ascending ? a[key] - b[key] : b[key] - a[key]
    );
    this._lastSort = { key, ascending };
  }

  public goToPage(page: number): void {
    this.pageRange.max = Math.ceil(
      (this.pagination.itemsCount / this.pagination.pageSize) * page
    );
    this.pageRange.min = this.pageRange.max - this.pagination.pageSize;
  }
}
